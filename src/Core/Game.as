class Game : Vendor
{
    bool initialised = false;

    CTrackMania@ app;
    CHmsViewport@ view;
    CHmsCamera@ camera;

    Game() {
        @app = cast<CTrackMania>(GetApp());
        @view = app.Viewport;
        VendorGame();
    }

    void VendorGame() { }

    void AddNods()
    {
        if (app.GameScene !is null && (app.CurrentPlayground !is null || app.Editor !is null) && view.Cameras.Length > 0) {
            AddVendorNods();
        }
    }

    void AddVendorNods()
    {
        InitNods();
    }

    void InitNods()
    {
        initialised = true;
        ApplySettings();
    }

    void RemoveNods()
    {
        initialised = false;
        RemoveVendorNods();
    }

    void RemoveVendorNods() { }

    void ApplySettings()
    {
        ApplyVendorSettings();
    }

    void ApplyVendorSettings() { }

    void Routine()
    {
        VendorRoutine();
    }

    void VendorRoutine() { }

    void Render()
    {
        if (Setting_FPS) {
            UI::Begin("\\$o\\$wFPS Counter", Setting_FPS, UI::WindowFlags::AlwaysAutoResize + UI::WindowFlags::NoTitleBar + UI::WindowFlags::NoDocking);
            UI::Text(tostring(int(game.view.AverageFps)));
            UI::End();
        }
    }

    void Update(float dt)
    {
        VendorUpdate(dt);
    }

    void VendorUpdate(float dt) { }

    UI::InputBlocking OnKeyPress(bool down, VirtualKey key)
    {
        bool block = false;
        if (key == Setting_ZClipShortcutKey && Setting_ZClipShortcut != Shortcut::Disabled) {
            if (Setting_ZClipShortcut == Shortcut::Hold) {
                Setting_ZClip = down ? false : true;
            } else if (Setting_ZClipShortcut == Shortcut::HoldReverse) {
                Setting_ZClip = down ? true : false;
            } else if (Setting_ZClipShortcut == Shortcut::Toggle && down) {
                Setting_ZClip = !Setting_ZClip;
                block = true;
            }
        }
        ApplySettings();
        return block ? UI::InputBlocking::Block : UI::InputBlocking::DoNothing;
    }
}
