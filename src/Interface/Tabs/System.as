class SystemTab : Tab
{
    string GetLabel(const string&in prefix = "") override { return Icons::Cog + prefix + " System"; }

    void Render() override
    {
		UI::Markdown("These settings affect **Default.json** config file, change at your own risk! They might not reset to their defaults even if you disable the setting, so please revert the setting back to its original value before disabling it. You can **Ctrl + Click** on the slider to change its value manually.");
		UI::Dummy(vec2(5, 5));

		Setting_System_GeomLodScaleZ_Enabled = UI::Checkbox("##Level of Detail", Setting_System_GeomLodScaleZ_Enabled);
		
		if (UI::IsItemHovered()) {
			UI::BeginTooltip();
			UI::Text("Distance before objects quality becomes lower");
			UI::Text("(higher value = lower distance and details)");
			UI::EndTooltip();
		}

		UI::SameLine();
		if (!Setting_System_GeomLodScaleZ_Enabled) UI::BeginDisabled();
		Setting_System_GeomLodScaleZ = UI::SliderFloat("Level of Detail", Setting_System_GeomLodScaleZ, System::GeomLodScaleZ_Min, System::GeomLodScaleZ_Max);
		UI::SameLine();
		UI::Text("(Current: " + game.view.SystemConfig.Display.GeomLodScaleZ + ")");
		if (!Setting_System_GeomLodScaleZ_Enabled) UI::EndDisabled();
	}
}