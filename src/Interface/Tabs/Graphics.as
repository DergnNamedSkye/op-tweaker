class GraphicsTab : Tab {
    string GetLabel(const string&in prefix = "") override { return Icons::PictureO + prefix + " Graphics"; }

	private string AddBlocks(int distance, int min, int max)
	{
		if (distance >= min && distance <= max) {
			int clamped = Math::Clamp(distance, min, max);
			float blocks = Math::Ceil(clamped / 32);
			string suffix = blocks == 1 ? "block" : "blocks";
			return clamped + "m (" + blocks + " " + suffix + ")";
		} else {
			return "Out of bounds?!";
		}
	}

    void Render() override
    {

		UI::Markdown("## Draw Distance");

		Setting_ZClip = UI::Checkbox("Enabled", Setting_ZClip);

		if (UI::IsItemHovered()) {
			UI::BeginTooltip();
			UI::Text("How far the game can render itself");
			UI::EndTooltip();
		}
		
#if TMNEXT
#if SIG_REGULAR
		bool canAsync = game.initialised && Setting_ZClip && !Setting_ZClipAsync && !cast<CVisionViewport>(game.view).AsyncRender;
		
		UI::SameLine();
		Setting_ZClipAsync = UI::Checkbox("Forced", Setting_ZClipAsync);

		if (UI::IsItemHovered()) {
			UI::BeginTooltip();
			UI::Text("\\$ff0Experimental Feature\n");
			UI::Text("Partially fixes Draw Distance being disabled on some open UI elements or modes (e.g. spectator).");
			UI::Text("Doesn't work in the editor and will stop working temporarily if you re-toggle this or the setting to the left.");
			if (canAsync) {
				UI::Text("\n\\$ff0Warning!");
				UI::Text("Toggling this option now won't apply changes immediately.");
			}
			UI::EndTooltip();
		}
#endif
#endif

		string converted = AddBlocks(Setting_ZClipDistance, ZClipDistance::Min, ZClipDistance::Max);
		Setting_ZClipDistance = UI::SliderInt("Distance", Setting_ZClipDistance, ZClipDistance::Min, ZClipDistance::Max, converted);

		if (UI::BeginCombo("Shortcut", tostring(Setting_ZClipShortcut))) {
			if (UI::Selectable("Disabled", false)) {
				Setting_ZClipShortcut = Shortcut::Disabled;
			}
			if (UI::Selectable("Hold", false)) {
				Setting_ZClipShortcut = Shortcut::Hold;
			}
			if (UI::Selectable("HoldReverse", false)) {
				Setting_ZClipShortcut = Shortcut::HoldReverse;
			}
			if (UI::Selectable("Toggle", false)) {
				Setting_ZClipShortcut = Shortcut::Toggle;
			}
			UI::EndCombo();
		}

		if (Setting_ZClipShortcut != Shortcut::Disabled) {
			if (UI::BeginCombo("Key", tostring(Setting_ZClipShortcutKey))) {
				for (int i = 1; i < 255; i++) { // 255 is length of VirtualKey
					if (tostring(VirtualKey(i)) == tostring(i)) continue; // thanks to NaNInf
					if (UI::Selectable(tostring(VirtualKey(i)), false)) {
						Setting_ZClipShortcutKey = VirtualKey(i);
					}
				}
				UI::EndCombo();
			}
		}

		if (Setting_ZClipDistance < 100) {
			UI::Separator();
			UI::Text("\\$f00Caution!");
			UI::Markdown("Draw Distance is **too low**.");
		}

#if TMNEXT
#if SIG_REGULAR
		if (canAsync) {
			UI::Separator();
			UI::Text("\\$ff0Warning!");
			UI::Markdown("Draw Distance is currently not being applied immediately.");
			UI::Markdown("You can partially fix that by turning on **Forced** setting.");
			UI::Markdown("(this is a bug which appears on some open menu elements or when you are in spectator mode)");
		}
#endif
#endif
	}
}