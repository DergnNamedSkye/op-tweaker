namespace DefaultColors
{
    vec4 Green = vec4(0, 1.25f, 0, 1);

    array<vec4> Button = {
        Text::ParseHexColor("#4296F963"), // Button
	    Text::ParseHexColor("#4296F9"),   // ButtonHovered
	    Text::ParseHexColor("#0F87F9")    // ButtonActive
    };
}

[SettingsTab name="Options" icon="Wrench"]
void SettingsTab_Main()
{

    if (!Setting_EmbeddedInterface || Setting_EmbeddedFirstTime) {
        if (Setting_EmbeddedFirstTime) {
            UI::Markdown("**Tweaker** is moving to **Scripts** menu " + Icons::Kenney::Heart);
            UI::Dummy(vec2(0, 0));
            UI::PushStyleColor(UI::Col::Button, DefaultColors::Button[0] * DefaultColors::Green);
            UI::PushStyleColor(UI::Col::ButtonHovered, DefaultColors::Button[1] * DefaultColors::Green);
            UI::PushStyleColor(UI::Col::ButtonActive, DefaultColors::Button[2] * DefaultColors::Green);
            if (UI::Button("Use Tweaker in a separate window")) {
                Setting_EmbeddedFirstTime = false;
                Setting_EmbeddedInterface = false;
                window.isOpened = true;
            }
            UI::PopStyleColor(3);
            if (UI::Button("Keep Tweaker in this window")) {
                Setting_EmbeddedFirstTime = false;
                Setting_EmbeddedInterface = true;
            }
        } else {
            UI::Markdown("Use **Scripts** sub-menu to access **Tweaker** settings.");
            UI::Markdown("(you can change the behavior in **Plugin** tab)");
        }
    } else {
        window.RenderTabContents(true);
    }
}

[SettingsTab name="Help" icon="QuestionCircle"]
void SettingsTab_Help()
{
    UI::Markdown("**Q:** How do I toggle Draw Distance using a keyboard key (or a controller button)?");
    UI::Markdown("A: Go to Draw Distance - select Toggle (or Hold) shortcut and choose your preferred key to map to.");
    UI::Dummy(vec2(5, 5));
    UI::Markdown("**Q:** How do I enter the value manually?");
    UI::Markdown("A: You can hold Ctrl and click on a slider.");
#if TMNEXT
    UI::Dummy(vec2(5, 5));
    UI::Markdown("**Q:** My Draw Distance disables itself when UI is hidden.");
    UI::Markdown("A: Go to Openplanet - Settings - General (first tab on the left) - disable \"Hide overlay on hidden game UI\".");
    UI::Dummy(vec2(5, 5));
    UI::Markdown("**Q:** My Draw Distance and/or Geometry Quality is stuck from the previous update.");
    UI::Markdown("A: Go to Legacy tab and enable \"Legacy Defaults\", then restart the game.");
#else
    UI::Dummy(vec2(5, 5));
    UI::Markdown("**Q:** I see black ground when playing environments other than Stadium.");
    UI::Markdown("A: Go to Environment tab and turn on \"Full SkyDome\" option (needs to be applied manually for now).");
#endif
}

#if TMNEXT
[SettingsTab name="Legacy" icon="Cog"]
void SettingsTab_Legacy()
{
    Setting_LegacyDefaults = UI::Checkbox("Legacy Defaults", Setting_LegacyDefaults);

    if (UI::IsItemHovered()) {
        UI::BeginTooltip();
        UI::Text("Resets ZClip and GeomLodScaleZ settings");
        UI::EndTooltip();
    }
}
#endif

[SettingsTab name="Plugin" icon="PuzzlePiece"]
void SettingsTab_Plugin()
{
    Setting_EmbeddedInterface = UI::Checkbox("Hide menu entry in Scripts and use this window only", Setting_EmbeddedInterface);
    Setting_EmbeddedFirstTime = UI::Checkbox("Show first time screen in Options tab", Setting_EmbeddedFirstTime);
}

#if SIG_REGULAR
[SettingsTab name="Preview" icon="Star"]
void SettingsTab_Preview()
{
#if TMNEXT
    string textVendor = "If you have **Club Access** you";
#else
    string textVendor = "You";
#endif

    string textFull = textVendor + " can download upcoming unsigned **0.3.0** version [here](https://openplanet.dev/plugin/tweakerpreview) (or just search \"Tweaker Preview\" in the **Plugin Manager**). Keep in mind it __will__ conflict with your current version, therefore you must uninstall this version first. Use at your own risk!";

    UI::Markdown(textFull);
}
#endif
